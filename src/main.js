// import md5 from "./node_modules/blueimp-md5/js/md5.js"

var form = document.querySelector('form')
var outs = Array.from(document.querySelectorAll('output'))

form.addEventListener('reset', (evt) => {
    // Prevent resetting original example values
    evt.preventDefault()
    // Clear named input values
    Array.from(form.querySelectorAll('input[name]'))
        .forEach(elt => elt.value = '')
})

form.addEventListener('submit', (evt) => {

    // Prevent regular submit request to page URL
    evt.preventDefault()

    // Get form data
    var form = evt.target
    var data = new FormData(form)
    var ents = Array.from(data.entries())
    var map = Object.fromEntries(ents)

    // Construct query
    var base =
        `https://admin.dataportal.se/store/search`
    
    // Entryscape/Solr required encodings
    var predEnc = map.predicate && md5(map.predicate).slice(0, 8)
    var val = map.object.replace(/:/g, '\\:')
    
    // Assemble query string
    var expr =
        predEnc ? `metadata.predicate.uri.${predEnc}:${val}`
        : val ? `metadata.object.uri:${val}`
        : ''
    var url =
        [
            encodeURI(base),
            [
                ['type', 'solr'],
                ['rdfFormat', 'application/ld+json'], // Does not have effect when the value is URI encoded
            ]
            .map(pair => pair.join('='))
            .concat(
                [
                    ['query', expr]
                ]
                .map(pair => pair.map(encodeURIComponent).join('='))
            )
            .join('&')
        ].join('?')

    outs[0].textContent = expr
    outs[1].innerHTML = `<a target="_blank" href="${url}">${url}</a>`
        
})
